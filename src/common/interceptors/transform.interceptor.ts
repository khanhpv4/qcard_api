import { CallHandler, ExecutionContext, Injectable, NestInterceptor, HttpStatus } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Result<T> {
  result: T;
}

@Injectable()
export class TransformInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next
      .handle()
      .pipe(map(result => {
        const ctx = context.switchToHttp();
        const res = ctx.getResponse();
        res.statusCode = HttpStatus.OK;
        return {
          statusCode: HttpStatus.OK,
          message: "Success",
          isError: false,
          result
        }
      }));;
  }
}
