import { Module } from '@nestjs/common';
import { AuthenticationModule } from './modules/authentication/authentication.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RestModule } from './modules/rest/rest.module';

const DBConfig = TypeOrmModule.forRoot({
  type: 'mysql',
  host: '103.221.223.106',
  port: 3306,
  username: 'admin_qcard',
  password: 'khanhpoly12@',
  database: 'admin_qcard',
  entities: [__dirname + '/entities/*.entity{.ts,.js}'],
  logging: false,
  synchronize: false,
});

@Module({
  imports: [
    DBConfig,
    AuthenticationModule,
    RestModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule { }
