import { Module } from '@nestjs/common';
import { RestController } from './controllers/rest/rest.controller';
import { PassportModule } from '@nestjs/passport';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'bearer' }),
  ],
  controllers: [RestController],
  exports: [PassportModule]
})
export class RestModule {}
