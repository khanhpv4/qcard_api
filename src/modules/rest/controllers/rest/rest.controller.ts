import { Controller, UseGuards, Get } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Controller('rest')
@UseGuards(AuthGuard())
export class RestController {
  @Get('test')
  async test(): Promise<any> {
    return "OK"
  }
}
