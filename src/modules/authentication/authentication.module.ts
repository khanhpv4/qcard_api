import { Module } from '@nestjs/common';
import { AuthController } from './controllers/auth/auth.controller';
import { AuthService } from './services/auth/auth.service';
import { UserService } from './services/user/user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Users } from '../../entities'
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { HttpStrategy } from './passport/http.strategy';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Users
    ]),
    PassportModule.register({ defaultStrategy: 'bearer' }),
    JwtModule.register({
      secret: 'S3CR3&K#@#$&*!!~',
      signOptions: {
        expiresIn: 3600,
      }
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, UserService, HttpStrategy],
  exports: [PassportModule]
})
export class AuthenticationModule { }
