import { Injectable, BadRequestException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from '../user/user.service';
import { Users } from '../../../../entities';
import { JwtPayload, LoginRequest } from '../../dto';
import { StringUtil } from '../../../../utils/StringUtil';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {
    // empty constructor
  }

  async validateUser(payload): Promise<any> {
    return await this.jwtService.verifyAsync(payload)
  }

  async login(req: LoginRequest): Promise<any> {
    try {
      const user = await this.userService.findByEmail(req.email);
      if (!user || user.password !== StringUtil.hashString(req.password)) {
        throw new BadRequestException({ error: "Username or password is invalid." });
      }

      return this.createResponseForLogin(user);
    } catch (error) {
      throw error;
    }
  }

  /**
   * Create response body for login
   * @param user 
   */
  private createResponseForLogin(user: Users): any {
    let payload: JwtPayload = {
      userId: user.id,
      email: user.email,
      name: user.name
    }
    const accessToken = this.jwtService.sign(payload);
    return {
      expiresIn: 3600,
      accessToken,
      profile: {
        id: user.id,
        email: user.email,
        name: user.name
      }
    }
  }
}
