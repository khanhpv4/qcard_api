import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm'
import { Users } from '../../../../entities'
import { Repository } from 'typeorm';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(Users)
    private usersRepository: Repository<Users>
  ) {
    // empty constructor
  }

  async findByEmail(email: string): Promise<Users> {
    try {
      return await this.usersRepository.findOne({
        where: {
          email: email
        }
      })
    } catch (error) {
      throw error;
    }
  }
}
