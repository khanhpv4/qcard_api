import { Controller, Body, Post, UseGuards } from '@nestjs/common';
import { AuthService } from '../../services/auth/auth.service';
import { LoginRequest } from '../../dto';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {
    //empty contructor
  }

  @Post('login')
  async login(@Body() req: LoginRequest): Promise<any> {
    try {
      return await this.authService.login(req);
    } catch (error) {
      throw error;
    }
  }
}
