export class JwtPayload {
  readonly userId: number;
  readonly email: string;
  readonly name: string;
}