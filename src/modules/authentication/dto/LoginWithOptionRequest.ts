import { IsString, IsNotEmpty } from 'class-validator';

export class LoginWithOptionRequest {
  @IsString()
  @IsNotEmpty()
  readonly token: string;
}