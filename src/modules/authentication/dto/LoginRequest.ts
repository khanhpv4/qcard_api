import { IsString, IsNotEmpty } from 'class-validator';

export class LoginRequest {
  @IsString()
  @IsNotEmpty()
  readonly email: string;

  @IsString()
  @IsNotEmpty()
  readonly password: string;
}