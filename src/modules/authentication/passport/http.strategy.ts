import { Strategy } from 'passport-http-bearer';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from '../services/auth/auth.service';

@Injectable()
export class HttpStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    super();
  }

  async validate(payload, done: Function) {
    try {
      const user = await this.authService.validateUser(payload);
      if (!user) {
        return done(new UnauthorizedException('Token is invalid.'), false);
      }

      done(null, user);
    } catch (error) {
      throw new UnauthorizedException('Unauthorized', error.message);
    }
  }
}