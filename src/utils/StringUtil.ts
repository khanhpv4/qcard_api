import * as crypto from 'crypto';

export class StringUtil {

  static hashString(value: string): String {
    return crypto.createHmac('md5', value).digest('hex');
  }
}