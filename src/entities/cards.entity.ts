import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("cards",{schema:"admin_qcard" } )
export class cards {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"id"
        })
    id:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"c_name"
        })
    c_name:string;
        

    @Column("int",{ 
        nullable:false,
        name:"c_type"
        })
    c_type:number;
        

    @Column("int",{ 
        nullable:false,
        name:"c_status"
        })
    c_status:number;
        

    @Column("text",{ 
        nullable:false,
        name:"c_content"
        })
    c_content:string;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"created_at"
        })
    created_at:Date | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"updated_at"
        })
    updated_at:Date | null;
        
}
