import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("users",{schema:"admin_qcard" } )
@Index("users_email_unique",["email",],{unique:true})
export class users {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:true,
        length:191,
        name:"name"
        })
    name:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:191,
        name:"email"
        })
    email:string;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"email_verified_at"
        })
    email_verified_at:Date | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"password"
        })
    password:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"remember_token"
        })
    remember_token:string | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"created_at"
        })
    created_at:Date | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"updated_at"
        })
    updated_at:Date | null;
        
}
