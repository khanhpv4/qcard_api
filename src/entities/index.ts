import { cards as Cards } from './cards.entity'
import { cart_tempts as CartTempts } from './cart_tempts.entity'
import { faqs as Faqs } from './faqs.entity'
import { fonts as Fonts } from './fonts.entity'
import { migrations as Migrations } from './migrations.entity'
import { password_resets as PasswordResets } from './password_resets.entity'
import { posts as Posts } from './posts.entity'
import { users as Users } from './users.entity'
import { versions as Versions } from './versions.entity'

export {
  Cards,
  CartTempts,
  Faqs,
  Fonts,
  Migrations,
  PasswordResets,
  Posts,
  Users,
  Versions
}