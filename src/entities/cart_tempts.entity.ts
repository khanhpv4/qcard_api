import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("cart_tempts",{schema:"admin_qcard" } )
export class cart_tempts {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"id"
        })
    id:string;
        

    @Column("int",{ 
        nullable:false,
        name:"u_id"
        })
    u_id:number;
        

    @Column("int",{ 
        nullable:false,
        name:"c_id"
        })
    c_id:number;
        

    @Column("int",{ 
        nullable:false,
        name:"t_type"
        })
    t_type:number;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"created_at"
        })
    created_at:Date | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"updated_at"
        })
    updated_at:Date | null;
        
}
