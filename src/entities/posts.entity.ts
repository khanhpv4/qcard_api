import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("posts",{schema:"admin_qcard" } )
export class posts {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"id"
        })
    id:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"p_title"
        })
    p_title:string;
        

    @Column("text",{ 
        nullable:false,
        name:"p_content"
        })
    p_content:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"p_avatar"
        })
    p_avatar:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"p_description"
        })
    p_description:string;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"created_at"
        })
    created_at:Date | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"updated_at"
        })
    updated_at:Date | null;
        
}
