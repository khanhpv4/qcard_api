import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { TransformInterceptor } from './common/interceptors';
import { AllExceptionsFilter } from './common/filters';
import { ValidationPipe } from './common/pipes';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // use interceptor
  app.useGlobalInterceptors(new TransformInterceptor());
  // use filter
  app.useGlobalFilters(new AllExceptionsFilter());
  // use pipe
  app.useGlobalPipes(new ValidationPipe());
  // set prefix /api/...
  app.setGlobalPrefix("/api")
  // enable cors
  app.enableCors({ allowedHeaders: "*" })

  await app.listen(3000);
}

bootstrap();
